from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

rooms = [
    {'id': 0, 'name': "Python"},
    {'id': 2, 'name': "Django"},
    {'id': 3, 'name': "Docker"},
]


def home(request):
    #return HttpResponse("Home Page")
    context = {'rooms': rooms}
    return render(request, 'base/home.html', context)


def room(request):
    #return HttpResponse("Room")
    return render(request, 'base/room_old.html')






